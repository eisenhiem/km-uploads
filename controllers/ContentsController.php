<?php

namespace app\controllers;

use app\models\Contents;
use app\models\ContentsSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContentsController implements the CRUD actions for Contents model.
 */
class ContentsController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Contents models.
     *
     * @return string
     */
    public function actionIndex($group_id)
    {
        $searchModel = new ContentsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        $dataProvider->query->andWhere(['group_id' => $group_id]);
        if(in_array(Yii::$app->user->identity->role,[4,5]) || Yii::$app->user->isGuest){
            $dataProvider->query->andWhere(['file_status' => '1']);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'group_id' => $group_id,
        ]);
    }

    /**
     * Displays a single Contents model.
     * @param int $content_id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($content_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($content_id),
        ]);
    }

    /**
     * Creates a new Contents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($group_id)
    {
        $model = new Contents();
        $model->group_id = $group_id;
        $model->user_id = Yii::$app->user->identity->id; 

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->file = $model->uploadFile($model, 'file');
                $model->files = $model->uploadMultiple($model,'files');
                if($model->save()){
                    return $this->redirect(['index','group_id' => $model->group_id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Contents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $content_id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($content_id)
    {
        $model = $this->findModel($content_id);

        if ($model->load(Yii::$app->request->post())) {

            $model->file = $model->uploadFile($model, 'file');
            $model->files = $model->uploadMultiple($model,'files');
            if($model->save()){
                return $this->redirect(['index','group_id' => $model->group_id]);
            }

        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionActive($content_id)
    {
        $model = $this->findModel($content_id);
        $model->file_status = '1';
        $model->save();

        return $this->redirect(['index','group_id' => $model->group_id]);
    }    
    
    public function actionDisable($content_id)
    {
        $model = $this->findModel($content_id);
        $model->file_status = '0';
        $model->save();

        return $this->redirect(['index','group_id' => $model->group_id]);
    }

    /**
     * Deletes an existing Contents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $content_id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($content_id)
    {
        $model = $this->findModel($content_id);
        @unlink(Yii::getAlias('@webroot').'/'.$model->uploadPath.'/'.$model->file);
        $model->delete();

        Yii::$app->session->setFlash('success', 'ลบข้อมูลเรียบร้อยแล้ว');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Contents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $content_id ID
     * @return Contents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($content_id)
    {
        if (($model = Contents::findOne(['content_id' => $content_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
