<?php

use kartik\icons\Icon;
use app\models\Groups;

Icon::map($this);

$group = Groups::find()->where(['is_active' => '1'])->all();

$items = [];

foreach($group as $g) {
    array_push($items,['label' => $g->group_name, 'url' => ['/contents/index','group_id' => $g->group_id], 'icon' => 'dot-circle',]);
}

?>

<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <div style="text-align: center;background-color:#396EB0;">
        <a href="<?=\yii\helpers\Url::home() ?>" class="brand-link">
            <!-- <img src="@web/images/logo.jpg" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
            <span class="brand-text font-weight-bold" style="color: white;">คลังความรู้</span>
        </a>
    </div>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <?php
            echo \hail812\adminlte\widgets\Menu::widget([
                'items' => [
                    [
                        'label' => 'Menu', 'icon' => 'dot-circle', 'iconClassAdded' => 'text-danger','visible' => true,
                        'items' => $items
                    ],
                    [
                        'label' => 'Admin Menu',
                        'icon' => 'cogs',
                        'badge' => '<span class="right badge badge-info">2</span>',
                        'visible' => in_array(Yii::$app->user->identity->role,[1,2,3]),
                        'items' => [
                            ['label' => 'ตั้งค่าผู้ใช้', 'url' => ['/user/admin/index'], 'icon' => 'users','visible' => Yii::$app->user->identity->role == 1, ],
                            ['label' => 'ตั้งค่าหมวด', 'url' => ['/groups/index'], 'icon' => 'list','visible' => in_array(Yii::$app->user->identity->role,[1,2]),],
                            ['label' => 'Gii',  'icon' => 'file-code', 'url' => ['/gii'], 'target' => '_blank', 'iconClassAdded' => 'text-warning', 'visible' => Yii::$app->user->identity->role == 1,],
                        ]
                    ],
                    ['label' => 'ลงชื่อเข้าใช้', 'url' => ['/user/security/login'], 'icon' => 'sign-in-alt', 'visible' => Yii::$app->user->isGuest],
                    ['label' => 'Logout', 'url' => ['/user/security/logout'], 'icon' => 'sign-out-alt', 'visible' => !Yii::$app->user->isGuest, 'template' => '&emsp;<a href="{url}" data-method="post">{icon}</a>'],
                ],
            ]);
            ?>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>