/*
Navicat MySQL Data Transfer

Source Server         : dataCenter
Source Server Version : 50505
Source Host           : 192.168.111.9:3306
Source Database       : km

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-08-10 15:40:35
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `configs`
-- ----------------------------
DROP TABLE IF EXISTS `configs`;
CREATE TABLE `configs` (
  `config_code` varchar(255) NOT NULL,
  `config_value` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`config_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of configs
-- ----------------------------

-- ----------------------------
-- Table structure for `contents`
-- ----------------------------
DROP TABLE IF EXISTS `contents`;
CREATE TABLE `contents` (
  `content_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `file_content` text DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `files` varchar(255) DEFAULT NULL,
  `filename` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `file_status` enum('0','1') DEFAULT '0',
  `d_update` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of contents
-- ----------------------------

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_name` varchar(255) DEFAULT NULL,
  `is_active` enum('1','0') DEFAULT '1',
  `d_update` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'นโยบาย/กฎหมาย/พรบ.', '1', '2022-08-10 15:07:26');
INSERT INTO `groups` VALUES ('2', 'ความรู้', '1', '2022-08-10 08:50:49');
INSERT INTO `groups` VALUES ('3', 'HA', '1', '2022-08-10 08:48:53');
INSERT INTO `groups` VALUES ('4', 'WI', '1', '2022-08-10 08:49:09');
INSERT INTO `groups` VALUES ('5', 'CPG', '1', '2022-08-10 15:03:53');
INSERT INTO `groups` VALUES ('6', 'CQI & R2R', '1', '2022-08-10 15:05:35');
INSERT INTO `groups` VALUES ('7', 'คู่มือ/แบบฟอร์ม', '1', '2022-08-10 15:06:33');
INSERT INTO `groups` VALUES ('8', 'อื่น ๆ', '1', '2022-08-10 15:06:47');

-- ----------------------------
-- Table structure for `profile`
-- ----------------------------
DROP TABLE IF EXISTS `profile`;
CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8mb3 DEFAULT NULL,
  `cid` varchar(13) CHARACTER SET utf8mb3 DEFAULT NULL,
  `position` varchar(100) CHARACTER SET utf8mb3 DEFAULT NULL,
  `position_level` varchar(30) CHARACTER SET utf8mb3 DEFAULT NULL,
  `line_id` varchar(255) CHARACTER SET utf8mb3 DEFAULT NULL,
  `phone_no` varchar(30) CHARACTER SET utf8mb3 DEFAULT NULL,
  `d_update` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of profile
-- ----------------------------
INSERT INTO `profile` VALUES ('1', 'นายจักรพงษ์ วงศ์กมลาไสย', '', 'นักวิชาการคอมพิวเตอร์ปฏิบัติการ', '', null, null, '2022-08-03 11:52:53');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) DEFAULT NULL,
  `is_active` enum('1','0') DEFAULT '1',
  `d_update` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'Admin', '1', '2022-08-10 08:22:05');
INSERT INTO `roles` VALUES ('2', 'Moderator', '1', '2022-08-10 08:05:22');
INSERT INTO `roles` VALUES ('3', 'Editor', '1', '2022-08-10 08:06:11');
INSERT INTO `roles` VALUES ('4', 'User', '1', '2022-08-10 08:06:41');

-- ----------------------------
-- Table structure for `token`
-- ----------------------------
DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL,
  UNIQUE KEY `token_unique` (`user_id`,`code`,`type`) USING BTREE,
  CONSTRAINT `token_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of token
-- ----------------------------

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `password_hash` varchar(60) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `auth_key` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT 0,
  `last_login_at` int(11) DEFAULT NULL,
  `role` int(2) DEFAULT 2,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_unique_username` (`username`) USING BTREE,
  UNIQUE KEY `user_unique_email` (`email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'lsk.hospital@gmail.com', '$2y$12$dI98GVzJ5Zyv1EJDYRcAeeuH0hlYklXZI7KiNrPggLe7bWo5zdue6', 'bylfadG2nkgPJcywRIF3fCjTuKhvNPKU', '1628149204', null, null, '::1', '1615301772', '1615301772', '0', '1660118576', '1');
