<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profile".
 *
 * @property int $user_id
 * @property string|null $fullname
 * @property string|null $cid
 * @property string|null $position
 * @property string|null $position_level
 * @property string|null $line_id
 * @property string|null $phone_no
 * @property string|null $d_update
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['d_update'], 'safe'],
            [['fullname', 'line_id'], 'string', 'max' => 255],
            [['cid'], 'string', 'max' => 13],
            [['position'], 'string', 'max' => 100],
            [['position_level', 'phone_no'], 'string', 'max' => 30],
            [['user_id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'fullname' => 'Fullname',
            'cid' => 'Cid',
            'position' => 'Position',
            'position_level' => 'Position Level',
            'line_id' => 'Line ID',
            'phone_no' => 'Phone No',
            'd_update' => 'D Update',
        ];
    }

}
