<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contents;

/**
 * ContentsSearch represents the model behind the search form of `app\models\Contents`.
 */
class ContentsSearch extends Contents
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content_id', 'group_id', 'user_id'], 'integer'],
            [['file_content', 'file', 'files', 'filename', 'file_status', 'd_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contents::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'content_id' => $this->content_id,
            'group_id' => $this->group_id,
            'user_id' => $this->user_id,
            'd_update' => $this->d_update,
        ]);

        $query->andFilterWhere(['like', 'file_content', $this->file_content])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'files', $this->files])
            ->andFilterWhere(['like', 'filename', $this->filename])
            ->andFilterWhere(['like', 'file_status', $this->file_status]);

        return $dataProvider;
    }
}
