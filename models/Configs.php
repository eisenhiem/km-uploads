<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "configs".
 *
 * @property string $config_code
 * @property string|null $config_value
 */
class Configs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'configs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['config_code'], 'required'],
            [['config_code', 'config_value'], 'string', 'max' => 255],
            [['config_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'config_code' => 'Config Code',
            'config_value' => 'Config Value',
        ];
    }
}
