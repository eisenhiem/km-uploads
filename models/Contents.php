<?php

namespace app\models;

use DateTime;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use \yii\web\UploadedFile;

/**
 * This is the model class for table "upload_files".
 *
 * @property int $content_id
 * @property int $group_id_id
 * @property string|null $file_content
 * @property string|null $file
 * @property string|null $files
 * @property string|null $filename
 * @property int|null $user_id
 * @property string|null $d_update
 * @property string|null $ีfile_status

 */
class Contents extends \yii\db\ActiveRecord
{
    public $upload_foler = 'uploads'; //ที่เก็บไฟล์
    public $uploadPath = 'uploads'; //ที่เก็บไฟล์

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contents';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'group_id'], 'integer'],
            [['d_update','file_content'], 'safe'],
            [['file_status'], 'string'],
            [
                ['file'], 'file',
                'extensions' => 'png,jpg,pdf,xlsx,docx,pptx,ppt,doc,xls',
                'skipOnEmpty' => true
            ],
            [
                ['files'], 'file',
                'extensions' => 'png,jpg,pdf,xlsx,docs,pttx,xls,doc,ptt',
                // 'maxSize' => '1024 * 1024 * 2',
                'maxSize' => 1024 * 1024 * 30,
                'maxFiles' => 3,
                'skipOnEmpty' => true
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'content_id' => 'ID',
            'group_id' => 'หมวด',
            'file_content' => 'หัวข้อ',
            'file' => 'ชื่อไฟล์ที่อัพโหลด',
            'files' => 'ชื่อไฟล์ที่อัพโหลด',
            'file_status' => 'สถานะนำเข้า',
            'user_id' => 'ผู้อัพโหลด',
            'd_update' => 'วันที่แก้ไขล่าสุด',
        ];
    }

    public function uploadFile($model, $attribute)
    {
        $file = UploadedFile::getInstance($model, $attribute);

        if ($file) {
            if ($this->isNewRecord) {
                $fileName = time() . '_' . $file->baseName . '.' . $file->extension;
            } else {
                $fileName = $this->getOldAttribute($attribute);
            }
            $file->saveAs(Yii::getAlias('@webroot') . '/' . $this->uploadPath . '/' . $fileName);

            return $fileName;
        }
        return $this->isNewRecord ? false : $this->getOldAttribute($attribute);
    }

    public function uploadMultiple($model, $attribute)
    {
        $files  = UploadedFile::getInstances($model, $attribute);
        //        $path = $this->uploadPath;
        if ($this->validate() && $files !== null) {
            $fileNames = [];
            foreach ($files as $file) {
                $fileName = time() . '_' . $file->baseName . '.' . $file->extension;
                if ($file->saveAs(Yii::getAlias('@webroot') . '/' . $this->uploadPath . '/' . $fileName)) {
                    $fileNames[] = $fileName;
                }
            }
            if ($model->isNewRecord) {
                return implode(',', $fileNames);
            } else {
                return implode(',', (ArrayHelper::merge($fileNames, $model->getOwnFilesToArray())));
            }
        }

        return $model->isNewRecord ? false : $model->getOldAttribute($attribute);
    }

    public function getOwnFilesToArray()
    {
        return $this->getOldAttribute('files') ? @explode(',', $this->getOldAttribute('files')) : [];
    }

    
    public function getProfile()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'user_id']);
    }

    
    public function getGroups()
    {
        return $this->hasOne(Groups::className(), ['group_id' => 'group_id']);
    }

    public function getDate()
    {
        $month = array("01"=>"ม.ค.","02"=>"ก.พ","03"=>"มี.ค.","04"=>"เม.ย.","05"=>"พ.ค.","06"=>"มิ.ย.","07"=>"ก.ค.","08"=>"ส.ค.","09"=>"ก.ย.","10"=>"ต.ค.","11"=>"พ.ย.","12"=>"ธ.ค.");
        $visit = new DateTime($this->d_update);
        $d = $visit->format('j');
        $m = $month[$visit->format('m')];
        $y = $visit->format('Y')+543;
        $h = $visit->format('H');
        $i = $visit->format('i');
        $s = $visit->format('s');
        $result = $d.' '.$m.' '.$y.' เวลา '.$h.':'.$i.':'.$s.' น.';
        return $result;
    }

}
