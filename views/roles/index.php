<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RolesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'บทบาทผู้ใช้งาน';
?>
<div class="roles-index">


    <p>
        <?= Html::a(Icon::show('plus').' เพิ่มบทบาท', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "บทบาทผู้ใช้งาน",
            'type' => GridView::TYPE_PRIMARY
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'role_id',
            'role_name',
            //'is_active',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'สถานะใช้งาน',
                //'filter' => [1=>"ACTIVE",0=>"DEACTIVE"],
                'template' => '{status}',
                'buttons' => [
                    'status' => function ($url, $model, $key) {
                        return $model->is_active == '1' ?
                            Html::a('ACTIVE', ['deactive', 'role_id' => $model->role_id], ['class' => 'btn btn-primary btn-block']) :
                            Html::a('DEACTIVE', ['active', 'role_id' => $model->role_id], ['class' => 'btn btn-danger btn-block']);
                    }
                ]
            ],
            //'d_update',
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a(Icon::show('edit'),['update','role_id' => $model->role_id], ['class'=>'btn btn-warning btn-block']);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
