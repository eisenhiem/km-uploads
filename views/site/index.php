<?php

use app\models\Contents;
use app\models\Groups;
use yii\helpers\Html;
use kartik\icons\Icon;

/** @var yii\web\View $this */

$group = Groups::find()->where(['is_active' => '1'])->all();

$this->title = 'คลังความรู้และข้อมูล';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4"><?= $this->title ?></h1>

        <p class="lead">โรงพยาบาลเหล่าเสือโก้ก</p>

    </div>

    <div class="body-content">

        <div class="row">

            <?php
            foreach ($group as $g) {
            ?>
                <div class="col-lg-4">
                    <div class="card card-info">
                        <div class="card-header">
                            <h2 align="center"><?= $g->group_name ?></h2>
                        </div>
                        <div calss="card-body">
                            <ul>
                                <?php
                                $list = Contents::find()->where(['group_id' => $g->group_id,'file_status' => '1'])->orderBy(['content_id' => SORT_DESC])->limit(5)->all();
                                foreach ($list as $l) {
                                    echo '<li>'.Html::a($l->file_content, ['contents/view', 'content_id' => $l->content_id],) . '</li>';
                                }
                                ?>
                            </ul>
                        </div>
                    </div>
                </div>

            <?php
            }
            ?>
        </div>

    </div>
</div>