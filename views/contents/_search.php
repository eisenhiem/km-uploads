<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ContentsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contents-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index','group_id'=>$group_id],
        'method' => 'get',
    ]); ?>
    <div class="row">
        <div class="col-8" style="text-align:right">
            <?= $form->field($model, 'file_content')->textInput(['placeholder' => 'ระบุหัวข้อ'])->label(false) ?>
        </div>
        <div class="col-4">
            <div class="form-group">
                <?= Html::submitButton(Icon::show('search').' ค้นหา', ['class' => 'btn btn-primary']) ?>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>