<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Contents */

$this->title = 'เพิ่มรายการ';

?>
<div class="contents-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
