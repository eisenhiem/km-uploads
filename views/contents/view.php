<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contents */

$this->title = $model->file_content;

\yii\web\YiiAsset::register($this);
?>
<div class="contents-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'group_id',
                'value' => function ($model) {
                    return $model->groups->group_name;
                }
            ],
            'file_content:ntext',
            [
                'attribute' => 'files',
                'value' => function ($model) {
                    if ($model) {
                        $path = $model->uploadPath;
                        // echo Yii::getAlias('@webroot') . '/' .$path. '/' . $files->files;
                        if ($model->file) {
                            $file = $model->file;
                            return 
                            Html::a($file,'../web/' . $path . '/' . $file, ['target' => '_blank']);
                        } else {
                            $result = '';
                            $mfile = explode(',', $model->files);
                            foreach ($mfile as $fileName) {
                                $result = $result . Html::a($fileName, '../web/' . $path . '/' . $fileName, ['target' => '_blank']) . "<br>";
                            }
                            return $result;
                        }
                    } else {
                        return '';
                    }
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->profile->fullname;
                }
            ],
            [
                'attribute' => 'file_status',
                'value' => function ($model) {
                    return $model->file_status == '1' ? 'เผยแพร่' : 'ไม่เผยแพร่';
                }
            ],
            [
                'attribute' => 'd_update',
                'value' => function ($model) {
                    return $model->getDate();
                }
            ],
        ],
    ]) ?>

</div>
