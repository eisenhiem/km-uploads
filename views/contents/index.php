<?php

use app\models\Groups;
use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

$g = Groups::find()->where(['group_id' => $group_id])->one();

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการ';

?>
<div class="contents-index">

    <p>
        <?= in_array(Yii::$app->user->identity->role, [1,3]) ? Html::a(Icon::show('plus') . ' เพิ่มรายการ', ['create','group_id' => $group_id], ['class' => 'btn btn-success']) : '' ?>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel,'group_id' =>$group_id]); 
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "หมวดหมู่ ".$g->group_name,
            'type' => GridView::TYPE_PRIMARY
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'content_id',
            //'group_id',
            'file_content:ntext',
            [
                'attribute' => 'files',
                'value' => function ($model) {
                    if ($model) {
                        $path = $model->uploadPath;
                        // echo Yii::getAlias('@webroot') . '/' .$path. '/' . $files->files;
                        if ($model->file) {
                            $file = $model->file;
                            return 
                            Html::a($file,'../web/' . $path . '/' . $file, ['target' => '_blank']);
                        } else {
                            $result = '';
                            $mfile = explode(',', $model->files);
                            foreach ($mfile as $fileName) {
                                $result = $result . Html::a($fileName, '../web/' . $path . '/' . $fileName, ['target' => '_blank']) . "<br>";
                            }
                            return $result;
                        }
                    } else {
                        return '';
                    }
                },
                'format' => 'raw',
            ],
            //'filename',
            //'user_id',
            //'file_status',
            'd_update',
            [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->profile->fullname;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'สถานะการเผยแพร่',
                //'filter' => [1=>"ACTIVE",0=>"DEACTIVE"],
                'template' => '{status}',
                'buttons' => [
                    'status' => function ($url, $model, $key) {
                        if(in_array(Yii::$app->user->identity->role, [1,2])){
                            return $model->file_status == '1' ?
                            Html::a('เผยแพร่', ['disable', 'content_id' => $model->content_id], ['class' => 'btn btn-success btn-block']) :
                            Html::a('ไม่เผยแพร่', ['active', 'content_id' => $model->content_id], ['class' => 'btn btn-danger btn-block']);
                        } else {
                            return '';
                        }
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'แก้ไข',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return in_array(Yii::$app->user->identity->role, [1,3]) ? Html::a(Icon::show('edit'),['update','content_id' => $model->content_id], ['class'=>'btn btn-warning btn-block']):'';
                    },
                ],
            ],
        ],
    ]); ?>


</div>