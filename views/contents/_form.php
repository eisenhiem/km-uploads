<?php

use kartik\file\FileInput;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contents-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1 col-xs-10 offset-xs-1">
            <div class="card card-primary">
                <div class="card-header">
                    <?= $form->field($model, 'file_content')->textInput() ?>
                </div>
                <div class="card-body">
                    <?= $form->field($model, 'files[]')->widget(FileInput::classname(), [
                        'options' => [
                            'multiple' => true
                        ],
                        'pluginOptions' => [
                            'allowedFileExtensions' => ['pdf', 'jpg','xlsx','xls','doc','docx','ppt','pptx'],
                            'showPreview' => true,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false,
                            'overwriteInitial' => false
                        ]
                    ]); ?>
                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <?= Html::submitButton(Icon::show('upload').' อัพโหลดไฟล์', ['class' => 'btn btn-success btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>