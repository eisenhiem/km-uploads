<?php

use kartik\grid\GridView;
use kartik\icons\Icon;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'หมวด';

?>
<div class="groups-index">

    <p>
    <?= Html::a(Icon::show('plus').' เพิ่มหมวด', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'panel' => [
            'heading' => "รายการหมวดหมู่",
            'type' => GridView::TYPE_PRIMARY
        ],
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'group_id',
            'group_name',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'สถานะใช้งาน',
                //'filter' => [1=>"ACTIVE",0=>"DEACTIVE"],
                'template' => '{status}',
                'buttons' => [
                    'status' => function ($url, $model, $key) {
                        return $model->is_active == '1' ?
                            Html::a('ACTIVE', ['disable', 'group_id' => $model->group_id], ['class' => 'btn btn-primary btn-block']) :
                            Html::a('DISABLE', ['active', 'group_id' => $model->group_id], ['class' => 'btn btn-danger btn-block']);
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a(Icon::show('edit'),['update','group_id' => $model->group_id], ['class'=>'btn btn-warning btn-block']);
                    },
                ],
            ],
        ],
    ]); ?>


</div>
