<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Profile */

$this->title = 'แก้ไขสิทธิ์การเข้าถึง: ' . $model->id;

?>
<div class="profile-update">

    <?= $this->render('_change', [
        'model' => $model,
    ]) ?>

</div>
