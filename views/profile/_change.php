<?php

use app\models\Roles;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

$role = ArrayHelper::map(Roles::find()->all(), 'role_id', 'role_name');
/* @var $this yii\web\View */
/* @var $model app\models\Profile */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="profile-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-8 offset-md-2 col-sm-10 offset-sm-1 col-xs-10 offset-xs-1">
            <div class="card card-primary">
                <div class="card-header">
                    สิทธิ์การใช้งาน
                </div>
                <div class="card-body">
                    <?= $form->field($model, 'role')->radioList($role)->label(false) ?>
                </div>
                <div class="card-footer">
                    <div class="form-group">
                        <?= Html::submitButton(Icon::show('save') . ' บันทึก', ['class' => 'btn btn-success btn-block']) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>