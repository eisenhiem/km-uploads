<?php

use kartik\icons\Icon;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfileSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลผู้ใช้';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-index">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'heading' => Icon::show('list') . "ข้อมูลผู้ใช้",
            'type' => GridView::TYPE_PRIMARY
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'user_id',
            [
                'attribute' => 'department_id',
                'value' => function($model){
                    return $model->department_id ? $model->department->department_name:'';
                }
            ],
            [
                'attribute' => 'sub_department_id',
                'value' => function($model){
                    return $model->sub_department_id ? $model->subDepartment->sub_department_name:'';
                }
            ],
            'fullname',
            [
                'header' => 'สิทธิ์',
                'value' => function($model){
                    if($model->user->role == 9){
                        return 'Admin/ประธาน RM/เลขา RM';
                    }
                    if($model->user->role == 5){
                        return 'RM Man/หัวหน้ากลุ่ม/หัวหน้าฝ่าย';
                    }
                    if($model->user->role == 1){
                        return 'ผู้ใช้ทั่วไป';
                    } else {
                        return 'ไม่ระบุ';
                    }
    
                }
            ],
                //'position_level',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => '',
                'options' => ['style' => ['width' => '120px']],
                'buttonOptions' => ['class' => 'btn btn-sm'],
                'template' => '{update} {change}',
                'buttons' => [
                    'update' => function ($url, $model, $key) {
                        return Html::a(Icon::show('edit'), ['update', 'user_id' => $model->user_id], ['class' => 'btn btn-success', 'style' => ['width' => '44px']]);
                    },
                    'change' => function ($url, $model, $key) {
                        return Html::a(Icon::show('key'), ['change', 'id' => $model->user_id], ['class' => 'btn btn-warning', 'style' => ['width' => '44px']]);
                    },
                ]
            ],
        ],
    ]); ?>


</div>
